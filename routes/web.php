<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    if (Auth::check()) {
        return redirect('admin/articles');
    }
    else {
        return view('admin.login');
    }
})->name('login');

Route::post('/admin/login_do', [LoginController::class, 'authenticate']);
Route::get('/admin/exit_do', [LoginController::class, 'logout']);
