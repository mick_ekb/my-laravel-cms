<div class='title d-flex align-items-center'>
	<div class='breadcrumbs mr30'>
		<b class='mr10'>Путь:</b> <?= $breadcrumbs ?>
		<? if($siteRef){ ?><a href='<?= $siteRef ?>' target='_blank' title='Показать на сайте'><i class="fas fa-desktop"></i></a><? } ?>
	</div>
	<div class='ml-auto'>
		<? if($userAdmin['is_super']){ ?>
			<a href='<?= site_url("admin/".$controller."/config") ?>' class='btn btn-gray mr20'><i class="fas fa-cog"></i></a>
		<? } ?>		
	</div>
</div>
<? if(count($tabs)>1){ ?>
	<div class='tabs-titles-block'>
		<div class='d-flex'>
			<?
				$i=0;
				foreach($tabs as $item_one){				
					++$i;
			?>
					<a href='#<?= $item_one['id']; ?>' <?= $i==1?"class='active'":""; ?>><?= $item_one['name']; ?></a>
			<?
				}
			?>
		</div>
	</div>
<? } ?>
<div class='content-block'>
	<form action='<?= site_url("admin/".$controller."/save_profile") ?>' method='post' enctype='multipart/form-data'>
		<div class='tabs-block'>
			<?
				$i=0;
				foreach($tabs as $item_one){
					++$i;
			?>
					<div id='<?= $item_one['id']; ?>' class='<?= $i==1?"":"d-none"; ?> tab-one'>
						<? if(isset($item_one['fields'])){ ?>
							<? foreach($item_one['fields'] as $field_name=>$field_view){ ?>
								<div class='field-<?= $field_name ?>'>
									<?= $field_view; ?>
								</div>
							<? } ?>
						<? }else{ ?>
							<?= $item_one['fullTab']; ?>
						<? } ?>
					</div>
			<?
				}
			?>
		</div>
		
		<div class='fixed-actions d-flex'>
			<input type='hidden' name='c_tab' value=''>
			<input type='hidden' name='c_controller' value='<?= $controller; ?>'>
			<input type='hidden' name='item_id' value='<?= $item_id ?>'>
			<input type='submit' class='btn btn-green mr50' value='Сохранить' name='save'>
			<input type='submit' class='btn btn-green2 mr20' value='Сохранить и закрыть' name='save_close'>
			
			<a href='<?= site_url("admin/".$controller."/show/".$pid) ?>' class='btn btn-red ml-auto'>Закрыть</a>
		</div>
	</form>
</div>
