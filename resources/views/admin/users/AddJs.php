<script>
	$(document).ready(function(){
		$('.content-block form').submit(function(){
			if($('.content-block form input[type=password]').length){
				let $pwd=$('.content-block form input[type=password]').first();
				if($pwd.val()){
					let $pwd_repeat=$('#'+$pwd.attr('id')+'_repeat');
					if($pwd_repeat.val()!=$pwd.val()){
						alert('Пароли не совпадают');
						return false;
					}
				}
				else{
					alert('Заполните пароль');
					return false;
				}
			}
		});
	});
</script>