<?
	$data=[
		'Просмотр'=>'show',
		'Добавление'=>'add',
		'Редактирование'=>'edit',
		'Удаление'=>'delete',
	];
	$group_name="";
	foreach($modules as $item_one){
		$group_name_new=$item_one['rus_group']?$item_one['rus_group']:$item_one['rus'];
		if($group_name!=$group_name_new){
?>
			<h4><?= $group_name_new; ?></h4>
<?
			$group_name=$group_name_new;
		}
?>
		<div class='fs15 color-red'><?= $item_one['rus']; ?></div>
		<div class='d-flex mb20 field-block align-items-center'>
			<? foreach($data as $rus=>$en){ ?>
				<div class='mr10'>
					<?= $rus; ?>:
				</div>
				<div class='mr40'>
					<?= form_dropdown("modules[{$item_one['modules_id']}][{$en}]", ['inherit' =>'Наследовать', 'allow'=>'Разрешить', 'disallow'=>'Запретить', ], $item_one[$en]?$item_one[$en]:"inherit"); ?>
				</div>
			<? } ?>
		</div>
<?
	}
?>