<ul>
    @foreach($menus as $item_one)
		<li data-name='<?= $item_one['rus']; ?>'>
			<a href='@if($item_one['module']=='#') $item_one['module'] @else url("admin/{$item_one['module']}") @endif'><i class="{{ $item_one['icon'] }} fs20"></i>  {{ $item_one['rus'] }}</a>
            @if(count($item_one['items']))
				<div class='popup'>
					<a href='#' class='close'><i class="fas fa-times"></i></a>
					<div class='header'>{{ $item_one['rus'] }}</div>
					<ul>
                        @foreach($item_one['items'] as $item_one2)
							<li><a href='{{ url("admin/{$item_one2['module']}") }}'>{{ $item_one2['rus'] }}</a></li>
                        @endforeach
					</ul>
				</div>
            @endif
		</li>
    @endforeach
	@if($user['is_super'])
		<li class='super'>
			<a href='{{ url("admin/supers") }}' class='active'><i class="fas fa-cog fs20"></i> Суперадмин</a>
		</li>
    @endif
</ul>
