<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $meta_title }}</title>

    <link rel="stylesheet" href="/css/bootstrap.css" >
    <link rel="stylesheet" href="/css/fontawesome.css">
    <link rel="stylesheet" href="/css/jquery-ui.theme.css">
    <link rel="stylesheet" href="/css/bootstrap4-toggle.css">
    <link rel="stylesheet" href="/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/select2-bootstrap4.css">
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/css/cropper.css">
    <link rel="stylesheet" href="/css/init.css">
    <link rel="stylesheet" href="/css/admin.style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery Version 1.11.1 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootbox.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.mjs.nestedSortable.js"></script>
    <script type="text/javascript" src="/js/bootstrap4-toggle.min.js"></script>
    <script type="text/javascript" src="/js/jquery.tree.controller.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <script type="text/javascript" src="/js/select2.full.js"></script>
    <script type="text/javascript" src="/js/select2.ru.js"></script>
    <script type="text/javascript" src="/js/dropzone.js"></script>
    <script type="text/javascript" src="/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="/js/cropper.min.js"></script>
    <script type="text/javascript" src="/js/jquery-cropper.js"></script>
    <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
    <script type="text/javascript" src="/js/admin.scripts.js"></script>
    {!! $js !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body class="@if(Session::has('adminLeftBar')&&Session::get('adminLeftBar')==0) small-sidebar @endif">
<header class='d-flex'>
    <a class='logo text-center fs24' href='{{ url("/admin") }}'>CMS</a>
    <div class='top-menu'>
        @yield('topMenu')
    </div>
    <div class='user'>
        <a href='{{ url("admin/users/profile") }}'><i class="fas fa-user mr10"></i> {{ $userAdmin['login'] }}</a>
    </div>
    <div class='exit-me'>
        <a href='{{ url("admin/users/exit_do") }}'><i class="fas fa-sign-out-alt fs24"></i></a>
    </div>
</header>
<aside class='left-side'>
    <div class="left-side-overlay"></div>
    {!! $leftMenu !!}
    <a href='#' class='hide-sidebar'><i class="fas fa-angle-left"></i></a>
    <div class='copyright'>&copy;2012-{{ date('Y') }} <a href='#' target='blank' class='underline color-white ml10'>Документация</a></div>
</aside>
<div class='content'>
    @yield('content')
</div>

<!-- Modal -->
{!! $modals !!}

</body>
</html>
