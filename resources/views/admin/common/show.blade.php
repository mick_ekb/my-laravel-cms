@extends('admin.template')

@section('content')
    <div class='title d-flex align-items-center'>
        <div class='breadcrumbs mr30'>
            <b class='mr10'>Путь:</b> {!! $breadcrumbs !!}
            @if($siteRef)
                <a href='{{ $siteRef }}' target='_blank' title='Показать на сайте'><i class="fas fa-desktop"></i></a>
            @endif
        </div>
        <div class='ml-auto'>
            @if($userAdmin['is_super'])
                <a href='{{ url("admin/".$controller."/config") }}' class='btn btn-gray mr20'><i class="fas fa-cog"></i></a>
            @endif
            @if($permissions['add']&&!$is_tree)
                <a href='{{ url("/admin/{$controller}/add/{$pid}") }}' class='btn btn-green'><i class="fas fa-plus-circle"></i> Добавить</a>
            @endif
        </div>
    </div>
    <div class='d-flex content-block'>
        @if($isGroups||$is_tree)
            <div class='groups-block @if($is_tree) wide @endif'>
                <form class='search-block d-flex' action='{{ url("/admin/$controller/searchGroup") }}' method='post'>
                    <input type='text' name='search' placeholder='Поиск по категориям' class='autocomplete' data-ajax="{{ url("/admin/$controller/searchGroupAutocomplete") }}">
                    <button><i class="fas fa-search"></i></button>
                </form>
                <div class='groups-actions d-flex '>
                    <div class='custom-chekbox mr5'><input type='checkbox' id='catalog-toggle-all'><label for='catalog-toggle-all'></label></div>
                    <a href='#' class='expand-all mr5' title='Развернуть все категории'><i class="fas fa-expand-arrows-alt"></i></a>
                    <a href='#' class='collapse-all mr5' title='Свернуть все категории'><i class="fas fa-compress-arrows-alt"></i></a>
                    @if($permissions['edit'])<a href='#' class='show-all mr5 disabled' title='Скрыть/показать выделенные'><i class="fas fa-eye"></i></a>@endif
                    @if($permissions['delete'])<a href='#' class='delete-all mr5 disabled' title='Удалить выделенные'><i class="fas fa-trash-alt"></i></a>@endif
                    @if($permissions['add'])<a href='{{ url("/admin/$controller/add_group/$pid") }}' class='btn btn-green ml-auto add-current-group'><i class="fas fa-plus-circle"></i> Добавить категорию</a>@endif
                </div>
                <ul class='catalog-list ui-sortable root treebody treesort' id='catalog-list' data-get_nodes='{{ url("/admin/{$controller}/openNode") }}/' data-showURL="{{ url("/admin/{$controller}/show/") }}/" data-isShowURL="{{ url("/admin/{$controller}/showToggle/") }}/" data-moveURL="{{ url("/admin/{$controller}/moveTreeItems/") }}/" data-editURL="{{ url("/admin/{$controller}/edit_group/") }}/" data-addURL="{{ url("/admin/{$controller}/add_group/") }}/" data-deleteURL="{{ url("/admin/{$controller}/delete_group/") }}/" data-mainsURL="{{ url("/admin/mains/edit_group/{$mains_group_id}") }}/" data-canEdit='{{ $permissions['edit']?1:0 }}' data-canAdd='{{ $permissions['add']?1:0 }}' data-canDelete='{{ $permissions['delete']?1:0 }}'></ul>
                <div class='search-result d-none'></div>
            </div>
        @endif

        @if(!$is_tree)
            <div class='items-block'>
                <form action='{{ URL::current() }}' method='post' class='items-form'>
                    <table class='all-items' data-controller="{{ $controller }}" data-pid="{{ $pid }}" data-page_num="{{ $page_num }}">
                        <thead>{!! $thead !!}</thead>
                        <tbody>{!! $table !!}</tbody>
                        <tfoot>{!! $tfoot !!}</tfoot>
                    </table>
                    <input name='move_to' value='' type='hidden'>
                    {{ csrf_field() }}
                </form>
            </div>
        @endif
    </div>
@endsection
