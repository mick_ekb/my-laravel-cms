@extends('admin.template')

@section('content')
    <div class='title d-flex align-items-center'>
        <div class='breadcrumbs mr30'>
            <b class='mr10'>Путь:</b> {!! $breadcrumbs !!}
            @if($siteRef)
                <a href='{{ $siteRef }}' target='_blank' title='Показать на сайте'><i class="fas fa-desktop"></i></a>
            @endif
        </div>
        <div class='ml-auto'>
            @if($userAdmin['is_super'])
                <a href='{{ url("admin/".$controller."/config") }}' class='btn btn-gray mr20'><i class="fas fa-cog"></i></a>
            @endif
            @if($permissions['add']&&!$is_tree)
                <a href='{{ url("/admin/{$controller}/add/{$pid}") }}' class='btn btn-green'><i class="fas fa-plus-circle"></i> Добавить</a>
            @endif
        </div>
    </div>

    @if(count($tabs)>1)
        <div class='tabs-titles-block'>
            <div class='d-flex'>
                @foreach($tabs as $item_one)
                        <a href='#{{ $item_one['id'] }}' @if($loop->first) class='active' @endif>{{ $item_one['name'] }}</a>
                @endforeach
            </div>
        </div>
    @endif
    <div class='content-block'>
        <form action='{{ url("admin/".$controller."/save") }}' method='post' enctype='multipart/form-data'>
            <div class='tabs-block'>
                @foreach($tabs as $item_one)
                    <div id='{{ $item_one['id'] }}' class='@unless($loop->first) d-none @endunless tab-one'>
                        @if(isset($item_one['fields']))
                            @foreach($item_one['fields'] as $field_name=>$field_view)
                                <div class='field-{{ $field_name }}'>
                                    {{ $field_view }}
                                </div>
                            @endforeach
                        @else
                            {{ $item_one['fullTab'] }}
                        @endif
                    </div>
                @endforeach
            </div>

            <div class='fixed-actions d-flex'>
                <input type='hidden' name='c_tab' value=''>
                <input type='submit' class='btn btn-green mr50' value='Сохранить' name='save'>
                <input type='submit' class='btn btn-green2 mr20' value='Сохранить и закрыть' name='save_close'>

                <div class='error'></div>

                <a href='{{ url("admin/".$controller."/show/".$pid) }}' class='btn btn-red ml-auto'>Закрыть</a>
            </div>
        </form>
    </div>
@endsection
