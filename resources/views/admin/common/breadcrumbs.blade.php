@foreach($items as $item_one)
    @if ($loop->last && !$is_group)
			<span>{{ strip_tags($item_one['name']) }}</span>
    @else
			<a href='{{url("admin/$controllerName/show/{$item_one['id']}")}}'>{{strip_tags($item_one['name'])}}</a> /
    @endif
@endforeach
