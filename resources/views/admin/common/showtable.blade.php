@foreach($items as $item_one)
    <tr data-id='{{ $item_one->id }}'>
        <td>
            @if($permissions['edit'])<a href="#" class="move @if($order!='order'||$orderType!='asc') d-none @endif" title="Переместить"><i class="fas fa-arrows-alt-v"></i></a>@endif
        </td>
        <td>
            <div class='custom-chekbox'><input type='checkbox' id='items-{{ $item_one->id }}' name='actions[]' value='{{ $item_one->id }}' class='id-checkbox'><label for='items-{{ $item_one->id }}'></label></div>
            <input type='hidden' name='list[{{ $item_one->id }}][id]' value='{{ $item_one->id }}'>
        </td>
        @foreach($tdsRools as $db_name=>$str_one)
            @if(isset($item_one->$db_name))
                @if(strpos($item_one->$db_name,"</td>"))
                    {!! $item_one->$db_name !!}
                @else
                   <td>{!! $item_one->$db_name  !!}</td>
                @endif
            @else
                <td></td>
            @endif
        @endforeach
        <td class='text-center'>
            {!! implode(" ",$item_one->actions) !!}
        </td>
    </tr>
@endforeach
