<div class="modal fade" id="change-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Переместить в категорию</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class='tree-controller' data-get_nodes='{{ url("/admin/{$controller}/openNode") }}' data-pids="" data-var='catalogData' data-name='pid' data-is_one='{{ $is_one }}'></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				<button type="button" class="btn btn-danger confirm-movement">Подтвердить</button>
			</div>
		</div>
	</div>
</div>
