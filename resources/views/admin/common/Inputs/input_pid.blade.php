<div class='d-flex field-block'>
	<div class='field-name'>{{ $name }}</div>
	<div class='field-data'>
		<div class='tree-controller' data-get_nodes='{{ url("admin/".$controllerName."/openNode") }}' data-pids="{{ $value }}" data-is_one="{{ $is_one }}" data-var='catalogData' data-name='pid'></div>
	</div>
</div>
