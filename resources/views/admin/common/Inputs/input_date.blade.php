<div class='d-flex field-block'>
	<div class='field-name'><?= $name; ?></div>
	<div class='field-data'>		
		<div class="input-group date date-field " id="datepicker_<?= $db_name; ?>" data-target-input="nearest">
			<? 
				$data=[
					'type'  => 'text',
					'name'  => $db_name,
					'class' => 'datetimepicker-input',
					'data-target'=>'#datepicker_'.$db_name,
				];
				if(isset($required)&&$required){
					$data['required']='required';
				}
				if(isset($value)&&!is_null($value)){
					$data['value']=$value;
				}
				echo form_input($data); 
			?>
			<div class="input-group-append" data-target="#datepicker_<?= $db_name; ?>" data-toggle="datetimepicker">
				<div class="input-group-text"><i class="fa fa-calendar"></i></div>
			</div>
		</div>
	</div>
</div>