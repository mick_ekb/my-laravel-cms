<div class='d-flex field-block'>
	<div class='field-name'><?= $name; ?></div>
	<div class='field-data'>
		<? 
			$data="";
			if(isset($required)&&$required){
				$data.=" required ";
			}
			if(isset($method)&&$method=='tags'){
				$data.=" class='select2-ajax' ";
				$data.=" data-ajax--url='".site_url("admin/".$controllerName."/select_ajax/".$db_name)."' ";
				$data.=" multiple ";
				$db_name.="[]";
			}
			if(isset($method)&&$method=='search'){
				$data.=" class='select2-ajax' ";
				$data.=" data-ajax--url='".site_url("admin/".$controllerName."/select_ajax/".$db_name)."' ";
			}
			
			echo form_dropdown($db_name, isset($values)?$values:array(),isset($value)?$value:"",$data);
		?>
	</div>
</div>
