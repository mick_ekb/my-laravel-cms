<div class='d-flex field-block'>
	<div class='field-name'>{{ $name }}</div>
	<div class='field-data'>
		<div class="custom-chekbox mr5"><input type="checkbox" name='{{ $db_name }}' @if($value) checked @endif id="field_{{ $db_name }}"><label for="field_{{ $db_name }}"></label></div>
	</div>
</div>
