<div class='d-flex field-block'>
    <div class='field-name'>{{ $name }}</div>
    <div class='field-data'>
        {!! Form::text($db_name, $value, $options) !!}
    </div>
    <div class='field-renew align-items-center d-flex ml20'>
        <a href='#' class='translit'><i class="fas fa-sync-alt fs20"></i></a>
    </div>
</div>
