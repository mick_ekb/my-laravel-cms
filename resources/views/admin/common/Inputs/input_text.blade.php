<div class='d-flex field-block'>
	<div class='field-name'>{{ $name }}</div>
	<div class='field-data'>
        {!! Form::text($db_name, $value, $options) !!}
	</div>
</div>
