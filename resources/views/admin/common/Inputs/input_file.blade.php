<div class='d-flex field-block'>
	<div class='field-name'><?= $name; ?></div>
	<div class='field-data'>
		
		<? if(is_null($value)){ ?>
			<div class='custom-file'>
				<input type='file' name='<?= $db_name; ?>' id='file_<?= $db_name; ?>'>
				<label for="file_<?= $db_name; ?>" class='btn btn-red fs13'>Выбрать файл</label>
			</div>
		<? } else { ?>
			<div id='cropper_<?= $db_name; ?>' class='file-image <? if($crop['width']&&$crop['height']) echo "cropper-block"; ?> d-inline-block' data-x='<?= $crop['x'] ?>' data-y='<?= $crop['y'] ?>' data-width='<?= $crop['width'] ?>' data-height='<?= $crop['height'] ?>'>
				<img src="<?= $path.$value; ?>" class='img-fluid'>
			</div>
			<div class='custom-file d-none'>
				<input type='file' name='<?= $db_name; ?>' id='file_<?= $db_name; ?>'>
				<label for="file_<?= $db_name; ?>" class='btn btn-red fs13'>Выбрать файл</label>
			</div>
			<a href="#" class="delete-file ml20 fs18" title='Удалить файл' data-name='<?= $db_name; ?>' data-id='<?= $item_id ?>' data-url='/admin/<?= $controller ?>/delete_file/<?= $table ?>'><i class="fas fa-times"></i></a>
		<? } ?>
	</div>
</div>