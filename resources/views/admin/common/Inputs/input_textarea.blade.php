<div class='d-flex field-block'>
	<div class='field-name'><?= $name; ?></div>
	<div class='field-data'>
		<? 
			$data=[
				'name'  => $db_name,
			];
			if(isset($required)&&$required){
				$data['required']='required';
			}
			if(isset($value)&&!is_null($value)){
				$data['value']=$value;
			}
			echo form_textarea($data); 
		?>
	</div>
</div>