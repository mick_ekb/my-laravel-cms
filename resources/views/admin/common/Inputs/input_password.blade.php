<div class='d-flex field-block'>
	<div class='field-name'>{{ $name }}</div>
	<div class='field-data d-flex'>
        {!! Form::password($db_name, $options) !!}
        {!! Form::password('', ['id'=>'password_'.$db_name."_repeat", 'placeholder'=>'Повторите пароль',]) !!}
	</div>
</div>
