<div class='d-flex field-block'>
	<div class='field-name'>{{ $name }}</div>
	<div class='field-data'>
        {!! Form::textarea($db_name, $value, $options) !!}
	</div>
	<script type='text/javascript'>
		var editor_{{ $db_name }} = CKEDITOR.replace('editor_{{ $db_name }}');
		CKEDITOR.timestamp='ABCD';
		CKFinder.setupCKEditor(editor_{{ $db_name }}, '/js/ckfinder/');
	</script>
</div>
