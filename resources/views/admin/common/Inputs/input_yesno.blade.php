<div class='d-flex field-block'>
	<div class='field-name'><?= $name; ?></div>
	<div class='field-data'>
		<input type="checkbox" name='<?= $db_name; ?>' id="field_<?= $db_name; ?>" data-toggle="toggle" data-on="Да" data-off="Нет" data-onstyle="success" data-offstyle="danger" data-size="sm" <?= $value?"checked":""; ?>>
	</div>
</div>