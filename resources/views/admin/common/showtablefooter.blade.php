<tr class='search-tr'>
    <th colspan='{{ count($tdsRools) }}'>
        @if($permissions['add']||$permissions['edit']||$permissions['delete'])
            <div class='d-flex w-100'>
                <div class='ml0'>
                    <select name='action'>
                        @if($permissions['edit']) <option value='renew'>Обновить все позиции</option>@endif
                        @if($isGroups&&$permissions['edit']) <option value='move'>Переместить выбранные</option>@endif
                        @if($permissions['add']) <option value='copy'>Скопировать выбранные</option>@endif
                        @if($permissions['delete']) <option value='delete'>Удалить выбранные</option>@endif
                    </select>
                </div>
                <div class='ml10'>
                    <input type='submit' value='Применить' class='btn btn-danger' id='submitForm'>
                </div>
            </div>
        @endif
    </th>
    <th colspan='2' class='text-center'>
        Показать: <input type='text' name='changeAdminOnPage' value='{{ $adminOnPage }}' class='order-td changeAdminOnPage' onfocus="$(this).select()">
    </th>
    <th>
        <div class='d-flex pager align-items-center ml-auto mr-auto'>
            <a href='#' class='page-left mr5'><i class="fas fa-angle-left"></i></a>
            <input type='text' name='page' value='{{ $page_num }}' data-max='{{ $pagesCount }}' onfocus="$(this).select()">
            <a href='#' class='page-right ml5'><i class="fas fa-angle-right"></i></a>
        </div>
        <p class='text-center'>Страниц: <span class='pagesCountReal'>{{ $pagesCount }}</span></p>
    </th>
</tr>
