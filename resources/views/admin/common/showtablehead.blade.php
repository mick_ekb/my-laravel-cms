<tr class='search-tr'>
    <th></th>
    <th>
        <div class='clear-filters mr5 pl5'>
            <a href='#'><i class="fas fa-eraser"></i></a>
        </div>
    </th>
    @foreach($tdsRools as $td_field=>$item_one)
        @if($item_one['filter'])
            @if(isset($item_one['values']))
                <th>
                    {!! Form::select($td_field, array("all" => 'Все')+$item_one['values'], isset($filters[$td_field])??"all") !!}
                </th>
            @else
                <th>
                    {!! Form::text($td_field, $filters[$td_field]??"", ['class' => 'filter-field', 'placeholder' => "Поиск: {$item_one['name']}",]) !!}
                </th>
            @endif
        @else
            <th></th>
        @endif
    @endforeach
    <th>
        <div class='d-flex pager align-items-center ml-auto mr-auto'>
            <a href='#' class='page-left mr5'><i class="fas fa-angle-left"></i></a>
            <input type='text' name='page' value='{{ $page_num }}' data-max='{{ $pagesCount }}' onfocus="$(this).select()">
            <a href='#' class='page-right ml5'><i class="fas fa-angle-right"></i></a>
        </div>
    </th>
</tr>
<tr class='names-tr'>
    <th></th>
    <th><div class='custom-chekbox'><input type='checkbox' id='items-toggle-all'><label for='items-toggle-all'></label></div></th>
    @foreach($tdsRools as $td_field=>$item_one)
        @if($item_one['order'])
            <th><a href='#' class='sorted-a @if($order==$td_field) sorted-{{ $orderType }} @endif' data-sort='{{ $td_field }}' data-sorttype='@if($order==$td_field&&$orderType=='asc') desc  @else asc @endif'>{{ $item_one['name'] }}</a></th>
        @else
            <th>{{ $item_one['name'] }}</th>
        @endif
    @endforeach
    <th class='text-center'>Действия</th>
</tr>
