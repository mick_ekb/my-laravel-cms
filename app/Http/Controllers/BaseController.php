<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Module;
use App\Models\ModulesConfig;


class BaseController extends Controller
{
    protected $controllerName, $isGroups;
    protected $templateVars=[], $parentModel, $childModel, $settings, $config;
    private $templateBloks=['meta_title','content','modals','js','topMenu','leftMenu'];

    public function __construct()
    {
        $this->childModel = "\Modules\\".$this->controllerName."\\Entities\\".$this->controllerName;
        if($this->isGroups){
            $this->parentModel="\Modules\\".$this->controllerName."\\Entities\\".$this->controllerName."Group";
        }

        $this->templateVars = array_fill_keys($this->templateBloks,'');

        $this->config = ModulesConfig::firstOrCreate([
            'name'=>config(strtolower($this->controllerName).".configName"),
            'title'=>config(strtolower($this->controllerName).".moduleName"),
        ]);
        $this->config->initFields();

        $this->isGroups=$this->isGroups&&boolval($this->config->getField("settings.is_groups"));
    }

    protected function initTemplate()
    {
        $this->templateVars['userAdmin'] = Auth::user()->toArray();
        $this->templateVars['topMenu'] = $this->getTopMenu();
        $this->templateVars['leftMenu'] = User::find(Auth::user()->id)->getLeftMenu();
    }

    public function getTopMenu(){
        return "";
    }

    public function isPermission($permission) : bool{
        $module=Module::where('module', $this->controllerName)->first();
        $user=User::find(Auth::user()->id);
        $permissions = $user->getPermissions();
        return Auth::user()->is_super||(isset($permissions[$module->id][$permission])&&$permissions[$module->id][$permission]=='allow');
    }

    /*работа с деревом категория*/
    public function openNode(){
        $items=[];
        $ids=\request()->input('ids');

        if($this->isGroups){
            if($ids){
                foreach($ids as $id){
                    $items[$id]=$this->parentModel::find($id)->getCatalogsLevel($id,[],['id','name','pid','is_show','is_can_delete'],0);
                }
            }
        }
        return response()->json($items);
    }

    public function showToggle($pid=0){
        if(!$this->isPermission('edit')){
            return;
        }

        if($this->isGroups){
            if($pid){
                $item=$this->parentModel::find($pid);
                if($pid>1&&!empty($item)){
                    $item->is_show=!$item->is_show;
                    $item->save();
                }
            }
            else if(\request()->input('ids')){
                $items=$this->parentModel::findMany(\request()->input('ids'));
                if(count($items)){
                    foreach($items as $item_one){
                        $item_one->is_show=!$item_one->is_show;
                        $item_one->save();
                    }
                }
            }
        }
    }

    public function searchGroupAutocomplete(){
        $filter = [];
        $search=trim(\request()->input('q'));
        if($this->isGroups&&$search){
            $filter=$this->parentModel::where('name', 'like', "%{$search}%")->get();
        }

        return response()->json($filter);
    }

    public function searchGroup(){
       $search=trim(\request()->input('q'));
        if($this->isGroups&&$search){
            $items=$this->parentModel::where('name', 'like', "%{$search}%")->get();
            foreach($items as &$item_one){
                $item_one->path=$this->parentModel::find($item_one->id)->getNodesPath();
            }

            return view('admin.common.groupsSearch',[
                'items'=>$items,
                'controllerName'=>strtolower($this->controllerName),
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return redirect('/admin/'.strtolower($this->controllerName)."/show/1");
    }

    public function show($pid=0, $page_num=1){
        if(!$this->isPermission('show')){
            abort(404);
        }

        $this->initTemplate();

        $model=$this->isGroups?$this->parentModel:$this->childModel;
        $item=$model::findOrFail($pid);

        $this->templateVars['meta_title']=config(strtolower($this->controllerName).".moduleName");

        $initState=$item->initShowDisplay($this->config, [
            'edit'=>$this->isPermission('edit'),
            'show'=>$this->isPermission('show'),
            'add'=>$this->isPermission('add'),
            'delete'=>$this->isPermission('delete'),
        ]);
        if($initState){
            return redirect($initState);
        }

        $itemsCount=$item->getListCount();
        $pagesCount=ceil($itemsCount/\request()->session()->get('adminOnPage'.$this->controllerName));
        if($page_num<1||($page_num!=1&&$page_num>$pagesCount)){
            return redirect('/admin/'.strtolower($this->controllerName)."/show/".$pid);
        }


        $is_tree=(bool)$this->config->getField("settings.is_tree");
        $thead=$item->getShowTableHead($page_num, $pagesCount);
        $table=!$is_tree?$item->getShowTable($page_num):"";
        $tfoot=$item->getShowTableFooter($this->isGroups, $page_num, $pagesCount);

        $catalogs="";
        if($this->isGroups){
            $catalogs=$item->getCatalogs();
        }

        $data=[
            'breadcrumbs'=>$item->getLocalBreadcrumbs(),
            'siteRef'=>$item->getSiteRef(),
            'isGroups'=>$this->isGroups,
            'controller'=>strtolower($this->controllerName),
            'pid'=>$pid,
            'permissions'=>[
                'add'=>$this->isPermission('add'),
                'edit'=>$this->isPermission('edit'),
                'delete'=>$this->isPermission('delete'),
            ],
            'mains_group_id'=>$this->config->mains_group_id,
            'is_tree'=>$is_tree,
            'table'=>$table,
            'thead'=>$thead,
            'tfoot'=>$tfoot,
            'page_num'=>$page_num,
        ];

        $viewName='admin.common.show';

        if(view()->exists(strtolower($this->controllerName)."::admin.common.show")){
            $viewName=strtolower($this->controllerName)."::admin.common.show";
        }

        if(!\request()->ajax()){
            if($this->isGroups){
                $pidsInfo=$this->config->getField("fields_group.pid",true);
                $is_multipids=(bool)$this->config->getField("settings.is_multipids");
                $is_one=isset($pidsInfo['is_one'])&&$is_multipids?$pidsInfo['is_one']:1;

                $this->templateVars['modals']=view("admin.common.modalmove",[
                    'controller'=>strtolower($this->controllerName),
                    'is_one'=>$is_one,
                ])->render();
            }

            $this->templateVars['js']= view("admin.common.js",[
                'catalogs'=>$catalogs,
            ])->render();

            return view($viewName, array_merge($this->templateVars, $data));
        }
        else{
             return response()->json([
                'table'=>$table,
                'pagesCount'=>$pagesCount,
            ]);
        }
    }

    public function add_group($pid=0){
        if(!$this->isGroups){
            abort(404);
        }

        $item=$this->parentModel::findOrFail($pid);
        $data= $this->add_abstract($item, true);

        $viewName='admin.common.addGroup';
        if(view()->exists(strtolower($this->controllerName)."::admin.common.addGroup")){
            $viewName=strtolower($this->controllerName)."::admin.common.addGroup";
        }

        return view($viewName, array_merge($this->templateVars, $data));
    }

    public function add($pid=0){
        $item=$this->childModel::findOrFail($pid);
        $data=$this->add_abstract($item, false);

        $viewName='admin.common.add';
        if(view()->exists(strtolower($this->controllerName)."::admin.common.add")){
            $viewName=strtolower($this->controllerName)."::admin.common.add";
        }
        return view($viewName, array_merge($this->templateVars, $data));
    }

    protected function add_abstract($item, bool $is_group=false){
        if(!$this->isPermission('add')) {
            abort(404);
        }
        $this->initTemplate();

        $this->templateVars['meta_title']=config(strtolower($this->controllerName).".moduleName")." - Добавить";

        $model=$is_group?$this->parentModel:$this->childModel;
        $model=new $model();
        $model->pid=$item->id;
        $tabs=$model->getTabs($this->config, User::find(Auth::user()->id)->is_super, $is_group);
        $js=$model->get_editJS();

        $data=[
            'breadcrumbs'=>$item->getLocalBreadcrumbs(),
            'controller'=>strtolower($this->controllerName),
            'tabs'=>$tabs,
            'pid'=>$item->pid,
            'siteRef'=>$item->getSiteRef(),
            'js'=>$js,
            'permissions'=>[
                'add'=>$this->isPermission('add'),
                'edit'=>$this->isPermission('edit'),
                'delete'=>$this->isPermission('delete'),
            ],
            'is_tree'=>(bool)$this->config->getField("settings.is_tree"),
        ];

        return $data;
    }

    public function setOrder(){
        if(!$this->isPermission('edit')){
            return;
        }

        if(is_array(\request()->input('orders'))){
            foreach(\request()->input('orders') as $item_id=>$value){
                $this->childModel::find($item_id)->update([
                    'order'=>intval($value),
                ]);
            }
        }
    }

}
