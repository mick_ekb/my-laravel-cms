<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
     /**
     * Обработка попыток аутентификации.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'login' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json([
                'redirect'=>url('admin/article'),
            ]);
        }
        else{
            return response()->json([
                'message'=>'Неправильная пара логин/пароль '.$credentials->password,
            ]);
        }
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect('admin');
    }
}
