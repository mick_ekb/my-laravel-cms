<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ModulesPermission
 *
 * @property int $id
 * @property int|null $users_id
 * @property int|null $users_group_id
 * @property string $add
 * @property string $edit
 * @property string $show
 * @property string $delete
 * @property int $modules_id
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereModulesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereUsersGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesPermission whereUsersId($value)
 * @mixin \Eloquent
 */
class ModulesPermission extends Model
{
    use HasFactory;

    public $timestamps = false;
}
