<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function request;

/**
 * App\Models\Base
 *
 * @method static Builder|Base newModelQuery()
 * @method static Builder|Base newQuery()
 * @method static Builder|Base query()
 * @mixin \Eloquent
 */
class Base extends Model
{
    use HasFactory;

    protected $isGroups=false, $controllerName, $config, $filepath, $settings;
    protected $protectFields=false;
    protected $extraPidsTable = '', $extraPidsField = '', $extraPidsGroupField = '';

    public function __construct(){
        $this->filepath=$_SERVER['DOCUMENT_ROOT']."/upload/images/".$this->controllerName."/";
    }

    public function getParentsIds(): array
    {
        $items = DB::select(DB::raw("SELECT T2.id
		FROM (
			SELECT
				@r AS _id,
				(SELECT @r := pid FROM $this->table WHERE id = _id) AS pid,
				@l := @l + 1 AS lvl
			FROM
				(SELECT @r := $this->id, @l := 0) vars,
				$this->table m
			WHERE @r <> 0) T1
		JOIN $this->table T2
		ON T1._id = T2.id
		ORDER BY T1.lvl DESC;"));

        $results=[];
        foreach($items as $item_one){
            $results[]=$item_one->id;
        }
        return $results;
    }

    public function getChildrenIds(): array
    {
        $items = DB::select(DB::raw("WITH RECURSIVE tree AS ( SELECT id, pid, 1 as level FROM `{$this->table}` WHERE pid = '{$this->id}' UNION ALL SELECT p.id, p.pid, t.level + 1 FROM {$this->table} p JOIN tree t ON t.id = p.pid ) SELECT distinct(id) as id FROM tree"));
        $results=[];
        foreach($items as $item_one){
            $results[]=$item_one->id;
        }

        return $results;
    }

    public function getCatalogs($pid=""){
        if($pid){
            $opened=[];
            $pids=explode(",",$pid);
            foreach($pids as $pid_one){
                $opened=array_merge($opened,self::find($pid_one)->getParentsIds());
            }
            $opened=array_unique($opened);
        }
        else if($this->id==1) {
            $opened = [];
            $pid=1;
        }
        else{
            $pid=$this->id;
            $opened=$this->getParentsIds();
        }
        $whitelist=['id','name','pid','is_show','is_can_delete'];
        $items=$this->getCatalogsLevel(1, $opened, $whitelist, $pid);

        return json_encode($items);
    }

    public function getCatalogsLevel($pid,$opened,$whitelist,$base){
        $items = DB::table($this->table)->where('pid', $pid)->orderBy('order', 'asc')->get();
        $results=[];
        foreach($items as &$item_one){
            $item_one = array_intersect_key((array)$item_one, array_flip($whitelist));
            $subitems_count = DB::table($this->table)->where('pid', $item_one['id'])->count();

            $item_one['is_show']=(bool)$item_one['is_show'];

            if(in_array($item_one['id'],$opened)){
                $item_one['expand']=true;
                if($subitems_count){
                    $item_one['children']=$this->getCatalogsLevel($item_one['id'],$opened,$whitelist,$base);
                }
            }
            else{
                $item_one['expand']=false;
                if($subitems_count){
                    $item_one['needload']=true;
                }
            }

            if($item_one['id']==$base){
                $item_one['active']=true;
            }
            $results[]=$item_one;
        }
        return $results;
    }

    /* Параметры отображения в списке */
    private $adminShowOnPage=50, $adminShowOrder="order", $adminShowOrderType="asc", $adminShowFilters=[], $adminShowPermissions=[], $adminShowConfig, $adminShowTdRools=[];

    public function initShowDisplay(ModulesConfig $config, $permissions){
        $this->adminShowConfig=$config;
        $this->adminShowPermissions=$permissions;

        if(request()->has('action')&&request()->has('list')){
            $modelName="\Modules\\".$this->controllerName."\\Entities\\".$this->controllerName;

            if(request()->input('action')=='renew'){
                $updateFields=[];

                $tdsRools = $config->getField('fieldsTds');
                foreach($tdsRools as $db_name=>$data){
                    if(in_array($data['function'],['checkboxTD','inputTD','yesnoTD'])&&$db_name!='order'){
                        $updateFields[$db_name]=$data['function'];
                    }
                }

                if(count($updateFields)){
                    foreach(request()->input('list') as $id=>$listData){
                        $lineOne=$modelName::find($id);
                        foreach($updateFields as $updateField=>$updateFunction){
                            $lineOne->$updateField=in_array($updateFunction,['checkboxTD','yesnoTD'])?(isset($listData[$updateField])?1:0):$listData[$updateField];
                        }
                        $lineOne->save();
                    }
                }
            }
            else if(is_array(request()->input('actions'))){
                if(request()->input('action')=='move'&&request()->input('move_to')){
                    $move_to=explode(",",request()->input('move_to'));
                    foreach(request()->input('actions') as $id){
                        $lineOne=$modelName::find($id);
                        $current_pids=$lineOne->getAllPids();
                        $new_pids=array_diff($move_to, $current_pids);
                        if(count($new_pids)){
                            $lineOne->addAdditionalPids($config, $new_pids);
                        }
                    }
                }
                else if($this->request->getPost('action')=='delete'){
                    $this->model->deleteIds($this->model->find($this->request->getPost('actions')));
                }
            }

            return '/admin/'.strtolower($this->controllerName)."/show/".$this->id;
        }

        if(!request()->session()->has('adminOnPage'.$this->controllerName)){
            $this->adminShowOnPage=$config->getField("settings.onPage")??50;
            request()->session()->put('adminOnPage'.$this->controllerName, $this->adminShowOnPage);
        }
        if(!request()->session()->has("admin{$this->controllerName}Order")){
            $this->adminShowOrder=config(strtolower($this->controllerName).".defaultOrder");
            request()->session()->put("admin{$this->controllerName}Order", $this->adminShowOrder);
        }
        if(!request()->session()->has("admin{$this->controllerName}OrderType")){
            $this->adminShowOrderType="asc";
            request()->session()->put("admin{$this->controllerName}OrderType", $this->adminShowOrderType);
        }

        if(request()->has('adminOnPage')){
            $adminOnPage=intval(request()->input('adminOnPage'));
            if($adminOnPage>0){
                $this->adminShowOnPage=$adminOnPage;
                request()->session()->put('adminOnPage'.$this->controllerName, $adminOnPage);
            }
        }


        $filters=[];
        if(request()->has('filters')&&is_array(request()->input('filters'))){
            $filters= request()->input('filters');
        }
        else if(request()->session()->get("admin{$this->controllerName}Filters")) {
            $filters=unserialize(request()->session()->get("admin{$this->controllerName}Filters"));
        }

        $tdsRools = $config->getField('fieldsTds');
        foreach($tdsRools as $db_name=>$value){
            if($config->hasActiveField(false, $db_name)) {
                $this->adminShowTdRools[$db_name] = $value;
            }
        }

        $this->adminShowFilters=[];
        foreach($filters as $db_name=>$value){
            if( $config->hasActiveField(false, $db_name)&&isset($tdsRools[$db_name]['filter'])&&$tdsRools[$db_name]['filter']&&$value!==""&&$value!='all'){
                $this->adminShowFilters[$db_name]=$value;
            }
        }

        request()->session()->put("admin{$this->controllerName}Filters", serialize($this->adminShowFilters));

        if(request()->has('order')){
            if(isset($this->adminShowTdRools[request()->input('order')]['order'])&&$this->adminShowTdRools[request()->input('order')]['order']){
                request()->session()->put("admin{$this->controllerName}Order", request()->input('order'));
                $this->adminShowOrder= request()->input('order');
                if(in_array(request()->input('orderType'),['asc','desc'])){
                    request()->session()->put("admin{$this->controllerName}OrderType", request()->input('orderType'));
                    $this->adminShowOrderType= request()->input('orderType');
                }
            }
        }
    }

    public function getListCount(){
        $query=$this->getListSQL(true );

        return $query->first()->counter;
    }

    protected function getListSQL($is_count=false, $page_num=1){
        $table=str_replace("_groups","s",$this->table);
        $query= DB::table($table);

        if($this->id!=1){
            $pids = $this->getChildrenIds();
            $pids[]=$this->id;
            if(count($pids)){
                $query=$query->whereIn("$table.pid", $pids);
                if($this->extraPidsTable){
                    $query=$query->orWhereRaw("exists(select 1 from {$this->extraPidsTable} where {$this->extraPidsTable}.`{$this->extraPidsGroupField}` IN (".implode(",",$pids).") and {$this->extraPidsTable}.`{$this->extraPidsField}`={$table}.id)");
                }
            }
        }

        $query=$this->getListJoinSQL($query);
        $query=$this->getExtraConditionsSQL($query);

        $tdsRools=$this->adminShowConfig->getField('fieldsTds');
        foreach($this->adminShowFilters as $db_field=>$value){
            $fieldOne=$this->adminShowConfig->getField("fields.".$db_field, true);
            if($value) {
                if ($fieldOne['type'] == 'select') {
                    $query = $query->where("{$table}.{$db_field}", $value);
                } else {
                    $query = $query->where("{$table}.{$db_field}", 'like', $value);
                }
            }
        }

        if($is_count){
            $query=$query->select(DB::raw('count(*) as counter'));
        }
        else {
            $query = $query->select(DB::raw("{$table}.*"));
            if($this->adminShowOnPage) {
                $query = $query->limit($this->adminShowOnPage);
                $query = $query->offset(($page_num - 1) * $this->adminShowOnPage);
            }
            if ($this->adminShowOrder) {
                $query = $query->orderByRaw("{$table}." . $this->adminShowOrder." ".$this->adminShowOrderType);
            }
        }

        return $query;
    }

    protected function getListJoinSQL($query){
        return $query;
    }

    protected function getExtraConditionsSQL($query){
        return $query;
    }

    public function getList($page_num){
        $query=$this->getListSQL(false, $page_num);
        $items=$query->get();
        foreach($items as &$str_one){
            foreach($this->adminShowConfig->getField('fieldsTds') as $db_name=>$data){
                if(isset($str_one->$db_name)&&method_exists($this, $data['function'])){
                    $str_one->$db_name=$this->{$data['function']}($str_one,$db_name,$data, $this->adminShowPermissions);
                }
            }

            $str_one->actions=[];
            foreach($this->adminShowConfig->getField('actions') as $item_one){
                if(method_exists($this, "action_".$item_one)){
                    $method="action_".$item_one;
                    $str_one->actions[]=$this->{$method}($str_one, $this->adminShowPermissions);
                }
            }
        }
        return $items;
   }

    public function getShowTableHead($page_num, $pagesCount){
        return view('admin.common.showtablehead',[
            'tdsRools'=>$this->adminShowTdRools,
            'order'=>$this->adminShowOrder,
            'orderType'=>$this->adminShowOrderType,
            'page_num'=>$page_num,
            'pagesCount'=>$pagesCount,
        ])->render();
    }

    public function getShowTable($page_num){
        $items=$this->getList($page_num);

        return view('admin.common.showtable',[
            'items'=>$items,
            'permissions'=>$this->adminShowPermissions,
            'order'=>$this->adminShowOrder,
            'orderType'=>$this->adminShowOrderType,
            'tdsRools'=>$this->adminShowTdRools,
        ])->render();
    }

    public function getShowTableFooter($isGroups, $page_num, $pagesCount){
        return view('admin.common.showtablefooter',[
            'isGroups'=>$isGroups,
            'tdsRools'=>$this->adminShowTdRools,
            'permissions'=>$this->adminShowPermissions,
            'adminOnPage'=>$this->adminShowOnPage,
            'page_num'=>$page_num,
            'pagesCount'=>$pagesCount,
        ])->render();
    }

   /*хлебные крошки*/
   public function getLocalBreadcrumbs(){
        $is_group=(bool)strpos($this->table,'_groups');

        $path=[];
        if(!$is_group){
            $item=self::find($this->pid);
            $path=[['id'=>$item['id'], 'name'=> $item['name']??"Запись - {$item['id']}",]];

            if($this->isGroups){
                $parentModel ="\Modules\\".$this->controllerName."\\Entities\\".$this->controllerName."Group";
                $path=array_merge( $parentModel::find($item['pid'])->getNodesPath(),$path);
            }
        }
        else{
            $path=$this->getNodesPath();
        }

        return view('admin.common.breadcrumbs',[
            'items'=>$path,
            'controllerName'=>strtolower($this->controllerName),
            'is_group'=>$is_group,
        ])->render();
   }

   public function getNodesPath(){
        $ids=$this->getParentsIds();
        $items=[];
        if(count($ids)){
            $items=self::find($ids)->toArray();
            usort($items, function($item1, $item2) use ($ids) {
                return (array_search($item1['id'], $ids) > array_search($item2['id'], $ids));
            });
        }
        return $items;
   }

   public function getSiteRef(){
        //todo
        return "";
   }

    /*Методы отображения*/
    protected function showTD($item,$db_name,$data, $permissions){
        return isset($item->$db_name)?$item->$db_name:"";
    }

    protected function selectTD($item,$db_name,$data, $permissions){
        if(isset($data['values'])&&isset($item->$db_name)&&isset($data['values'][$item->$db_name])){
            return $data['values'][$item->$db_name];
        }
        return isset($item->$db_name)??"";
    }

    protected function checkboxTD($item,$db_name,$data, $permissions){
        $value=(bool)isset($item->$db_name)&&$item->$db_name;
        return view("admin.common.Tds.checkboxTD",[
            'value'=>$value,
            'db_name'=>$db_name,
            'item_id'=>$item->id,
        ])->render();
    }

    protected function inputTD($item,$db_name,$data, $permissions){
        $value=isset($item->$db_name)?$item->$db_name:"";
        return view("admin.common.Tds.inputTD",[
            'value'=>$value,
            'db_name'=>$db_name,
            'item_id'=>$item->id,
        ])->render();
    }

    protected function editTD($item,$db_name,$data, $permissions){
        $value=isset($item->$db_name)?$item->$db_name:"";
        if(!isset($permissions['edit'])||$permissions['edit']!='allow'){
            return $value;
        }
        return "<a href='/admin/".strtolower($this->controllerName)."/edit/{$item->id}'>".$value."</a>";
    }

    protected function yesnoTD($item,$db_name,$data, $permissions){
        $value=(bool)isset($item->$db_name)&&$item->$db_name;
        return view("admin.common.Tds.yesnoTD",[
            'value'=>$value,
            'db_name'=>$db_name,
            'item_id'=>$item->id,
        ])->render();
    }

    /*формат действий*/
    protected function action_edit($item, $permissions){
        if(!isset($permissions['edit'])||$permissions['edit']!='allow'){
            return "";
        }
        return "<a href='/admin/".strtolower($this->controllerName)."/edit/{$item->id}' class='edit mr10' title='Редактировать'><i class='fas fa-pencil-alt'></i></a>";
    }

    protected function action_delete($item, $permissions){
        if(!isset($permissions['delete'])||$permissions['delete']!='allow'||(isset($item->is_can_delete)&&!$item->is_can_delete)){
            return "";
        }
        return "<a href='/admin/".strtolower($this->controllerName)."/delete/{$item->id}' class='delete color-red confirm' data-confirm='Вы уверены, что хотите удалить?' title='Удалить'><i class='fas fa-times'></i></a>";
    }

    /*Создание и редактирование*/
    private $edit_js="";
    public function get_editJS(){
        return $this->edit_js;
    }

    public function getTabs($config, $is_super=false, $is_group=false){
        $this->config=$config;
        $fields=$config->getField($is_group?'fields_group':'fields');
        $tabs=[];
        foreach($fields as $tab=>$data){
            if(isset($data['fields'])&&count($data['fields'])){
                $tabs[]=[
                    'id'=>$tab,
                    'name'=>$data['name'],
                    'fields'=>$this->createFields($data['fields'], $is_group),
                ];
            }
            else if(isset($data['function'])&&method_exists($this, $tab)){
                $tabContent=$this->$tab();
                if($tabContent){
                    $tabs[]=[
                        'id'=>$tab,
                        'name'=>$data['name'],
                        'fullTab'=>$tabContent,
                    ];
                }
            }
        }
        return $tabs;
    }

    public function createFields($fields, $is_group){
        $data=[];
        foreach($fields as $db_name=>$item_one){
            $method='input_'.$item_one['type'];
            if(method_exists($this, $method)){
                $item_one['db_name']=$db_name;
                $item_one['controllerName']=strtolower($this->controllerName);
                $templateName=$method;
                if(view()->exists(strtolower($this->controllerName)."::admin.inputs.".$templateName)){
                    $viewName=strtolower($this->controllerName)."::admin.inputs.".$templateName;
                }
                else{
                    $viewName="admin.common.inputs.".$templateName;
                }

                $data[$db_name]=$this->$method($db_name,$item_one,$viewName, $is_group);
            }
        }
        return $data;
    }

    protected function input_text($db_name,$field,$viewName, $is_group){
         return view($viewName, [
            'name'=>$field['name'],
            'db_name'=>$db_name,
            'value'=>isset($item->$db_name)??"",
            'options'=>isset($field['required'])?["required"=>"required"]:[],
        ])->render();
    }

    protected function input_password($db_name,$field,$viewName, $is_group){
        $options=isset($field['required'])?["required"=>"required"]:[];
        $options['id']='password_'.$db_name;
        $options['placeholder']='Новый пароль';
        $options['class']='mr20';

        return view($viewName, [
            'name'=>$field['name'],
            'db_name'=>$db_name,
            'value'=>isset($item->$db_name)??"",
            'options'=>$options,
        ]);
    }

    protected function input_url($db_name,$field,$viewName, $is_group){
        $value='';
        if(!empty($item->id)){
            $url=SiteUrl::where(['module'=>$this->controllerName, 'external_id'=>$item->id, 'is_group'=>$is_group,])->first();
            $value=isset($url->value)??"";
        }

        $options=isset($field['required'])?["required"=>"required"]:[];
        $options['data-translit']='name';

        return view($viewName, [
            'name'=>$field['name'],
            'db_name'=>$db_name,
            'value'=>$value,
            'options'=>$options,
        ]);
    }

    public function search_select($db_field, $search, $item_id=null){
        return [];
    }

    protected function input_select($db_name,$field,$contentTemplate,$item){
        $field['value']=isset($item[$db_name])?$item[$db_name]:0;

        if(isset($field['method'])){
            if($field['method']=='function'&&method_exists($this, "select_".$db_name)){
                $methodDo="select_".$db_name;
                $field['values']=$this->$methodDo($db_name);
            }
            else if($field['method']=='search'&&isset($item[$db_name])&&$item[$db_name]){
                $values=$this->search_select($db_name, $item[$db_name], $item['id']);
                $field['values']=[];
                foreach($values as $item_one){
                    $field['values'][$item_one['id']]=$item_one['text'];
                }
            }
            else if($field['method']=='tags'&&method_exists($this, "select_values_".$db_name)&&isset($item['id'])){
                $methodDo="select_values_".$db_name;
                $field['values']=$this->$methodDo($db_name,$item['id']);
                $field['value']=array_keys($field['values']);
            }
            else{
                $field['values']=[];
            }
        }
        $field['required']=isset($field['required']);

        return view($contentTemplate, $field);
    }

    protected function input_checkbox($db_name,$field,$contentTemplate,$item){
        $field['required']=isset($field['required']);
        $field['value']=isset($item[$db_name])?$item[$db_name]:0;
        return view($contentTemplate, $field);
    }

    protected function input_yesno($db_name,$field,$contentTemplate,$item){
        $field['required']=isset($field['required']);
        $field['value']=isset($item[$db_name])?$item[$db_name]:1;
        return view($contentTemplate, $field);
    }

    protected function input_date($db_name,$field,$contentTemplate,$item){
        $field['value']=isset($item[$db_name])?$item[$db_name]:null;
        if($field['value']=='0000-00-00') $field['value']="";
        if($field['value']){
            $field['value']=date("d.m.Y",strtotime($field['value']));
        }
        $field['required']=isset($field['required']);
        return view($contentTemplate, $field);
    }

    protected function input_datetime($db_name,$field,$contentTemplate,$item){
        $field['value']=isset($item[$db_name])?$item[$db_name]:null;
        if($field['value']=='0000-00-00 00:00:00') $field['value']="";
        if($field['value']){
            $field['value']=date("d.m.Y H:i",strtotime($field['value']));
        }
        $field['required']=isset($field['required']);
        return view($contentTemplate, $field);
    }

    protected function input_pid($db_name,$field,$viewName,$is_group){
        $value=$this->pid;
        if(!empty($item->id)){
            $pids=$this->getAllPids();
            if(is_array($pids)){
                $value=implode(",",$pids);
            }
            else{
                $value=$pids;
            }
        }

        if($this->isGroups){
            $parentModel ="\Modules\\".$this->controllerName."\\Entities\\".$this->controllerName."Group";
            $this->edit_js.=view("admin.common.js",[
                'catalogs'=>$parentModel::find($value)->getCatalogs(),
            ])->render();
        }

        $is_multipids=(bool)$this->config->getField("settings.is_multipids");
        $is_one=isset($field['is_one'])&&$is_multipids?$field['is_one']:1;

        return view($viewName, [
            'name'=>$field['name'],
            'db_name'=>$db_name,
            'value'=>$value,
            'is_one'=>$is_one,
            'controllerName'=>strtolower($this->controllerName),
        ]);
    }

    public function getAllPids(){
        if($this->extraPidsTable){
            $pids=[$this->pid];
            $items = DB::table($this->extraPidsTable)->where([$this->extraPidsField=>$this->id,])->get();
            foreach($items as $item_one){
                $pids[] = $item_one[$this->extraPidsGroupField];
            }

            return $pids;
        }
        return $this->pid;
    }

    protected function input_textarea($db_name,$field,$contentTemplate,$item){
        $field['value']=isset($item[$db_name])?$item[$db_name]:null;
        $field['required']=isset($field['required']);
        return view($contentTemplate, $field);
    }

    protected function input_ckeditor($db_name,$field,$viewName,$is_group){
        $options=isset($field['required'])?["required"=>"required"]:[];
        $options['id'] = 'editor_'.$db_name;
        return view($viewName, [
            'name'=>$field['name'],
            'db_name'=>$db_name,
            'value'=>isset($item->$db_name)??"",
            'options'=>$options,
        ]);
    }

    protected function input_file($db_name,$field,$contentTemplate,$item){
        $field['value']=isset($item[$db_name])&&file_exists($this->filepath.$item[$db_name])&&$item[$db_name]?$item[$db_name]:null;
        $field['path']=str_replace($_SERVER['DOCUMENT_ROOT'],"",$this->filepath);
        $field['item_id']=isset($item['id'])?$item['id']:0;
        $field['controller']=strtolower($this->controllerName);
        $field['table']=$this->table;
        $field['crop']=[
            'x'=>0,
            'y'=>0,
            'width'=>0,
            'height'=>0,
        ];
        if(isset($item[$db_name."_info"])&&$item[$db_name."_info"]){
            $field['crop']=unserialize($item[$db_name."_info"]);
        }
        else if(isset($item[$db_name."_info"])){
            $info=$this->config->getField(strpos($this->table,"_group")?1:0,$db_name);
            if(isset($info['x_size'])&&$info['x_size']&&isset($info['y_size'])&&$info['y_size']){
                $field['crop']['width']=$info['x_size'];
                $field['crop']['height']=$info['y_size'];
            }
        }
        return view($contentTemplate, $field);
    }

    protected function input_gallery($db_name,$field,$contentTemplate,$item){
        $field['value']=null;
        $field['folder']="";
        if(isset($item[$db_name])){
            $filesTmp=explode(";",trim($item[$db_name]));
            $files=[];
            $field['folder']=$folder="/upload/images/".strtolower($this->controllerName)."_".$db_name;
            foreach($filesTmp as $file_one){
                if(trim($file_one)&&file_exists($_SERVER['DOCUMENT_ROOT'].$folder."/".$file_one)){
                    $files[]=$file_one;
                }
            }

            if(count($files)){
                $field['value']=implode(";",$files);
            }
        }

        $field['item_id']=isset($item['id'])?$item['id']:0;
        $field['table']=$this->table;
        $field['controller']=strtolower($this->controllerName);
        return view($contentTemplate, $field);
    }

    public function addAdditionalPids($config, $pids){
        $is_one=false;
        $field=$config->getField('fields.pid',true);
        $settings=$config->getField('settings');

        if($this->isGroups&&!strpos($this->table,"_groups")){
            $is_one=(isset($field['is_one'])&&isset($settings['is_multipids'])&&$settings['is_multipids'])?$field['is_one']:1;
        }

        if($this->extraPidsTable&&!$is_one){
            DB::table($this->extraPidsTable)->where([$this->extraPidsField=>$this->id,])->delete();
            foreach($pids as $item_one){
                DB::table($this->extraPidsTable)->insert([
                    $this->extraPidsField=>$this->id,
                    $this->extraPidsGroupField=>$item_one,
                ]);
            }
        }
        else{
            $this->pid=$pids[0];
            $this->save();
            $this->updatePath($pids[0]);
        }
    }

    public function updatePath($new_pid){
        $is_group=(bool)strpos($this->table,"_groups")!==false;
        $moduleInfo=ModulesConfig::where('name',$this->controllerName)->toArray();
        if($moduleInfo['mains_group_id']){
            if($is_group){
                $currentGroupUrl=$this->db->table('urls')->where(['module'=>$module."_group", "external_id" => $oldItem['id'],])->get()->getFirstRow('array');

                if(is_null($new_pid)){
                    $this->db->table('urls')->set(['modules_path'=>'/',])->where(['module'=>$module."_group", "external_id" => $oldItem['id'],])->update();
                    if($currentGroupUrl['modules_path']!='/'){
                        $from="{$currentGroupUrl['modules_path']}{$currentGroupUrl['value']}/";
                        $to="/{$currentGroupUrl['value']}/";

                        $this->db->query("
							UPDATE `urls`
							SET `modules_path` = INSERT(`modules_path`, LOCATE('{$from}', `modules_path`), CHAR_LENGTH('{$from}'), '{$to}')
							where `module`='{$module}' or `module`='{$module}_group' and `modules_path` LIKE '{$from}%'
						");
                    }
                }
                else{
                    $newGroupUrl=$this->db->table('urls')->where(['module'=>$module."_group", "external_id" => $new_pid,])->get()->getFirstRow('array');
                    if(isset($newGroupUrl['id'])){
                        $this->db->table('urls')->set(['modules_path'=>$newGroupUrl['modules_path'].$newGroupUrl['value']."/",])->where(['module'=>$module."_group", "external_id" => $oldItem['id'],])->update();

                        $from="{$currentGroupUrl['modules_path']}{$currentGroupUrl['value']}/";
                        $to="{$newGroupUrl['modules_path']}{$newGroupUrl['value']}/{$currentGroupUrl['value']}/";

                        $this->db->query("
							UPDATE `urls`
							SET `modules_path` = INSERT(`modules_path`, LOCATE('{$from}', `modules_path`), CHAR_LENGTH('{$from}'), '{$to}')
							where `module`='{$module}' or `module`='{$module}_group' and `modules_path` LIKE '{$from}%'
						");
                    }
                }
            }
            else{
                if(is_null($new_pid)){
                    $this->db->table('urls')->set(['modules_path'=>'/',])->where(['module'=>$module, "external_id" => $oldItem['id'],])->update();
                }
                else{
                    $groupUrl=$this->db->table('urls')->where(['module'=>$module."_group", "external_id" => $new_pid,])->get()->getFirstRow('array');
                    if(isset($groupUrl['id'])){
                        $this->db->table('urls')->set(['modules_path'=>$groupUrl['modules_path'].$groupUrl['value']."/",])->where(['module'=>$module, "external_id" => $oldItem['id'],])->update();
                    }
                }
            }
        }
    }

}
