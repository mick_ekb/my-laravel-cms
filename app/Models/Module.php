<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Module
 *
 * @property int $id
 * @property int|null $pid
 * @property string $rus
 * @property string $module
 * @property int $is_urls
 * @property string $icon
 * @property int $is_init
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|Module newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Module newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Module query()
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereIsInit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereIsUrls($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Module whereRus($value)
 * @mixin \Eloquent
 */
class Module extends Model
{
    use HasFactory;
    public $timestamps = false;
}
