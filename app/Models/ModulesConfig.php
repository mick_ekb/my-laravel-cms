<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Models\ModulesConfig
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $config
 * @property int $mains_group_id
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig whereMainsGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModulesConfig whereTitle($value)
 * @mixin \Eloquent
 */
class ModulesConfig extends Model
{
    use HasFactory;

    public $timestamps = false;

    private $activeFields=null, $activeFieldsGroup=null;
    public $defaultOrder="order";

    public function hasActiveField($is_group, $fieldName){
        $fields=$is_group?$this->activeFieldsGroup:$this->activeFields;
        foreach($fields as $tab=>$data){
            if(isset($data['fields'])&&count($data['fields'])){
                foreach($data['fields'] as $db_name=>$item_one){
                    if($db_name==$fieldName){
                        return $item_one;
                    }
                }
            }
        }
        return null;
    }

    public function getField($settingsName, bool $is_one=false){
        $parts=explode(".",$settingsName);
        if($parts[0]=='fields'){
            $baseField=$this->activeFields;
        }
        else if($parts[0]=='fields_group'){
            $baseField=$this->activeFieldsGroup;
        }
        else if($parts[0]=='settings'){
            $baseField=$this->config[$parts[0]];
        }
        else if(in_array($parts[0], ['actions', 'fieldsTds'])){
            return config(strtolower($this->name).".".$parts[0]);
        }
        else{
            return null;
        }

        if(count($parts)==1){
            return $baseField;
        }

        if($is_one){
            if(isset($parts[1])) {
                foreach ($baseField as $tabName => $tabFields) {
                    if (isset($tabFields['fields'])) {
                        foreach ($tabFields['fields'] as $db_name => $item_one) {
                            if ($db_name == $parts[1]) {
                                return $item_one;
                            }
                        }
                    }
                }
            }
            return null;
        }

        for($i=1;$i<count($parts);++$i){
            if(isset($baseField[$parts[$i]])){
                $baseField=$baseField[$parts[$i]];
            }
            else{
                return null;
            }
        }
        return $baseField;
    }

    public function initFields(){
        $is_super=Auth::user()->is_super??false;
        $actives=[];
        if($this->config){
            $actives_tmp=unserialize($this->config);
            $actives['fields']=[];
            foreach(config(strtolower($this->name).".fields") as $tabName=>$tabFields){
                if(isset($tabFields['fields'])){
                    foreach($tabFields['fields'] as $fieldName=>$fieldData){
                        if((isset($actives_tmp['fields'][$fieldName])&&$actives_tmp['fields'][$fieldName])||($is_super&&isset($fieldData['is_super_field'])&&$fieldData['is_super_field'])){
                            $actives['fields'][$fieldName]=true;

                            if(!isset($this->activeFields[$tabName])){
                                $this->activeFields[$tabName]=[
                                    'name'=>$tabFields['name'],
                                    'fields'=>[],
                                ];
                            }
                            $this->activeFields[$tabName]['fields'][$fieldName]=$fieldData;
                        }
                        else{
                            $actives['fields'][$fieldName]=false;
                        }
                    }
                }
                else if(isset($tabFields['function'])){
                    $this->activeFields[$tabName]=$tabFields;
                }
            }

            $actives['fields_group']=[];
            foreach(config(strtolower($this->name).".fields_group") as $tabName=>$tabFields){
                if(isset($tabFields['fields'])){
                    foreach($tabFields['fields'] as $fieldName=>$fieldData){
                        if((isset($actives_tmp['fields_group'][$fieldName])&&$actives_tmp['fields_group'][$fieldName])||($is_super&&isset($fieldData['is_super_field'])&&$fieldData['is_super_field'])){
                            $actives['fields_group'][$fieldName]=true;

                            if(!isset($this->activeFieldsGroup[$tabName])){
                                $this->activeFieldsGroup[$tabName]=[
                                    'name'=>$tabFields['name'],
                                    'fields'=>[],
                                ];
                            }
                            $this->activeFieldsGroup[$tabName]['fields'][$fieldName]=$fieldData;
                        }
                        else{
                            $actives['fields_group'][$fieldName]=false;
                        }
                    }
                }
                else if(isset($tabFields['function'])){
                    $this->activeFieldsGroup[$tabName]=$tabFields;
                }
            }

            $actives['settings']=[];
            foreach(config(strtolower($this->name).".settings") as $settingsName=>$settingsData){
                $actives['settings'][$settingsName]=isset($actives_tmp['settings'][$settingsName])?$actives_tmp['settings'][$settingsName]:false;
            }

            $this->config=$actives;
        }
        else{
            $actives['fields']=[];
            foreach(config(strtolower($this->name).".fields") as $tabName=>$tabFields){
                if(isset($tabFields['fields'])){
                    foreach($tabFields['fields'] as $fieldName=>$fieldData){
                        if((isset($fieldData['is_default'])&&$fieldData['is_default'])||($is_super&&isset($fieldData['is_super_field'])&&$fieldData['is_super_field'])){
                            $actives['fields'][$fieldName]=true;

                            if(!isset($this->activeFields[$tabName])){
                                $this->activeFields[$tabName]=[
                                    'name'=>$tabFields['name'],
                                    'fields'=>[],
                                ];
                            }
                            $this->activeFields[$tabName]['fields'][$fieldName]=$fieldData;
                        }
                        else{
                            $actives['fields'][$fieldName]=false;
                        }
                    }
                }
                else if(isset($tabFields['function'])){
                    $this->activeFields[$tabName]=$tabFields;
                }
            }

            $actives['fields_group']=[];
            foreach(config(strtolower($this->name).".fields_group") as $tabName=>$tabFields){
                if(isset($tabFields['fields'])){
                    foreach($tabFields['fields'] as $fieldName=>$fieldData){
                        if((isset($fieldData['is_default'])&&$fieldData['is_default'])||($is_super&&isset($fieldData['is_super_field'])&&$fieldData['is_super_field'])){
                            $actives['fields_group'][$fieldName]=true;

                            if(!isset($this->activeFieldsGroup[$tabName])){
                                $this->activeFieldsGroup[$tabName]=[
                                    'name'=>$tabFields['name'],
                                    'fields'=>[],
                                ];
                            }
                            $this->activeFieldsGroup[$tabName]['fields'][$fieldName]=$fieldData;
                        }
                        else{
                            $actives['fields_group'][$fieldName]=false;
                        }
                    }
                }
                else if(isset($tabFields['function'])){
                    $this->activeFieldsGroup[$tabName]=$tabFields;
                }
            }

            $actives['settings']=[];
            foreach(config(strtolower($this->name).".settings") as $settingsName=>$settingsData){
                $actives['settings'][$settingsName]=$settingsData['default'];
            }

            $this->config=$actives;

            $this->update([
                'config'=>serialize($actives),
                'mains_group_id'=>0,
            ]);
        }
    }

    public function setConfig($request){
        $db = \Config\Database::connect();
        $saves=[];
        if(!is_null($request->getPost('fields'))){
            $saves['fields']=$request->getPost('fields');
        }
        if(!is_null($request->getPost('fields_group'))){
            $saves['fields_group']=$request->getPost('fields_group');
        }
        if(!is_null($request->getPost('settings'))){
            $saves['settings']=$request->getPost('settings');
        }
        $db->table('modules_config')->set('config', serialize($saves))->where('name', $this->configName)->update();
    }

    public function getTds(){
        $fields=[];
        $settings=$this->getSettings();

        foreach($this->fieldsTds as $db_name=>$data){
            if(isset($data['field_visible'])){
                $info=$this->getField(0,$data['field_visible']);
                if(!is_null($info)){
                    if(isset($data['settings_visible'])&&!$settings[$data['settings_visible']]){
                        continue;
                    }

                    $data['type']=$info['type'];
                    if(isset($info['values'])){
                        $data['select_values']=$info['values'];
                    }

                    $fields[$db_name]=$data;
                }
            }
        }
        return $fields;
    }
}
