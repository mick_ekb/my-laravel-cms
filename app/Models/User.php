<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int|null $pid
 * @property string $login
 * @property string $password
 * @property string $name
 * @property string $name2
 * @property string $name3
 * @property int $is_super
 * @property int $is_can_delete
 * @property string $confirm
 * @property string $clear_password
 * @property string $email
 * @property string $phone
 * @property string $address_full
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $building
 * @property string $flat
 * @property string $discount
 * @property int $is_banned
 * @property string $file
 * @property string $file_info
 * @property string $date
 * @property int $order
 * @property int $is_show
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddressFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBuilding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereClearPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereConfirm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFileInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsBanned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsCanDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsSuper($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    private $permissions=[];

    public function getLeftMenu(){
        $user=Auth::user()->toArray();
        $permissions=$this->getPermissions();

        $menus = [];

        foreach(Module::where('pid',null)->orderBy('order')->get()->toArray() as $item_one)
        {
            if($item_one['module']!='#'){
                if($user['is_super']||(isset($permissions[$item_one['id']]['show'])&&$permissions[$item_one['id']]['show']=='allow')){
                    $item_one['items']=[];
                    $menus[]=$item_one;
                }
            }
            else{
                $submenus=[];
                foreach(Module::where('pid',$item_one['id'])->orderBy('order')->get()->toArray() as $item_one2){
                    if($user['is_super']||(isset($permissions[$item_one2['id']]['show'])&&$permissions[$item_one2['id']]['show']=='allow')){
                        $submenus[]=$item_one2;
                    }
                }
                if(count($submenus)){
                    $item_one['items']=$submenus;
                    $menus[]=$item_one;
                }
            }
        }

        return view('admin.users.left_menu',[
            'user'=>$user,
            'menus'=>$menus,
        ])->render();
    }

    public function getPermissions(){
        if(!empty($this->permissions)){
            return $this->permissions;
        }
        $permissions=[];

        foreach(ModulesPermission::Where('users_id', $this->id)->get()->toArray() as $item_one){
            $permissions[$item_one['modules_id']]=[
                'add'=>$item_one['add'],
                'edit'=>$item_one['edit'],
                'show'=>$item_one['show'],
                'delete'=>$item_one['delete'],
            ];
        }

        $pids=array_reverse(UserGroup::find($this->pid)->getParentsIds());
        foreach($pids as $pid_one){
            foreach(ModulesPermission::Where('users_group_id', $pid_one)->get()->toArray() as $item_one){
                $permissions[$item_one['modules_id']]=[
                    'add'=>isset($permissions[$item_one['modules_id']])&&$permissions[$item_one['modules_id']]['add']!='inherit'?$permissions[$item_one['modules_id']]['add']:$item_one['add'],
                    'edit'=>isset($permissions[$item_one['modules_id']])&&$permissions[$item_one['modules_id']]['edit']!='inherit'?$permissions[$item_one['modules_id']]['edit']:$item_one['edit'],
                    'show'=>isset($permissions[$item_one['modules_id']])&&$permissions[$item_one['modules_id']]['show']!='inherit'?$permissions[$item_one['modules_id']]['show']:$item_one['show'],
                    'delete'=>isset($permissions[$item_one['modules_id']])&&$permissions[$item_one['modules_id']]['delete']!='inherit'?$permissions[$item_one['modules_id']]['delete']:$item_one['delete'],
                ];
            }
        }
        $this->permissions = $permissions;
        return $this->permissions;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
