<?php

namespace Modules\Test\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TestController extends BaseController
{
    protected $controllerName="Test";
    protected $isGroups=true;
}
