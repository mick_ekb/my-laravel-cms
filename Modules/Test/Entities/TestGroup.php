<?php

namespace Modules\Test\Entities;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TestGroup extends Base
{
    use HasFactory;

    protected $fillable = [];

    protected $controllerName = 'Test';
    protected $isGroups=true;

    protected $extraPidsTable = 'test_pids';
    protected $extraPidsField = 'tests_id';
    protected $extraPidsGroupField = 'test_groups_id';

    protected static function newFactory()
    {
        return \Modules\Test\Database\factories\TestGroupFactory::new();
    }
}
