<?php

namespace Modules\Test\Entities;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Test extends Base
{
    use HasFactory;

    protected $fillable = [
        'pid','name','order','is_show','state','select1','select2','select4','checkbox','date','datetime','textarea',
        'ckeditor','file','file_info','file_info','meta_title'
    ];

    protected $controllerName = 'Test';
    protected $isGroups=true;

    protected $extraPidsTable = 'test_pids';
    protected $extraPidsField = 'tests_id';
    protected $extraPidsGroupField = 'test_groups_id';

    protected static function newFactory()
    {
        return \Modules\Test\Database\factories\TestFactory::new();
    }
}
