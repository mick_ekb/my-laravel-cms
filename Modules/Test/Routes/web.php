<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/test', 'middleware' => ['auth']], function(){
    Route::get('/', 'TestController@index');
    Route::match(['get', 'post'], '/show/{pid?}/{page_num?}', 'TestController@show');
    Route::post('/openNode', 'TestController@openNode');
    Route::post('/showToggle/{pid?}', 'TestController@showToggle');
    Route::post('/searchGroup', 'TestController@searchGroup');
    Route::post('/searchGroupAutocomplete', 'TestController@searchGroupAutocomplete');
    Route::post('/setOrder', 'TestController@setOrder');

    Route::match(['get', 'post'], '/add_group/{pid?}', 'TestController@add_group');
    Route::match(['get', 'post'], '/add/{pid?}', 'TestController@add');
    Route::match(['get', 'post'], '/edit_group/{pid?}', 'TestController@edit_group');
    Route::match(['get', 'post'], '/edit/{pid?}', 'TestController@edit');
});
