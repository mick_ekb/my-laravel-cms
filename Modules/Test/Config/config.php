<?php

return [
    'moduleName'  => 'Тестовый модуль',
    'configName'  => 'Test',
    'defaultOrder' => "order",
    'fields'=>[
        'tab1'=>[
            'name'=>'Информация',
            'fields'=>[
                'name'=>[
                    'name'=>'Текстовое поле',
                    'type'=>'text',
                    'required'=>true,
                    'is_default'=>true,
                ],
                'url'=>[
                    'name'=>'URL',
                    'type'=>'url',
                    'is_default'=>true,
                ],
                'select1'=>[
                    'name'=>'Выпадающий список',
                    'type'=>'select',
                    'values'=>[
                        '0'=>'Не выбрано',
                        '1'=>'Первый пункт',
                        '2'=>'Второй пункт',
                    ],
                    'is_default'=>true,
                ],
                'select2'=>[
                    'name'=>'Список с поиском',
                    'type'=>'select',
                    'method'=>'search',
                ],
                'select3'=>[
                    'name'=>'Список с тегами',
                    'type'=>'select',
                    'method'=>'tags',
                ],
                'select4'=>[
                    'name'=>'Выпадающий список 4',
                    'type'=>'select',
                    'method'=>'function',
                ],
                'checkbox'=>[
                    'name'=>'Галочка',
                    'type'=>'checkbox',
                ],
                'is_show'=>[
                    'name'=>'Показать',
                    'type'=>'yesno',
                ],
                'order'=>[
                    'name'=>'Порядок',
                    'type'=>'text',
                ],
                'date'=>[
                    'name'=>'Дата',
                    'type'=>'date',
                    'save_default'=>'NOW',
                ],
                'datetime'=>[
                    'name'=>'Дата и время',
                    'type'=>'datetime',
                ],
                'pid'=>[
                    'name'=>'Категории',
                    'type'=>'pid',
                ],
                'textarea'=>[
                    'name'=>'Текст (многоcтрочный)',
                    'type'=>'textarea',
                ],
                'ckeditor'=>[
                    'name'=>'Редактор',
                    'type'=>'ckeditor',
                    'is_default'=>true,
                ],
            ]
        ],
        'tab2'=>[
            'name'=>'Изображения',
            'fields'=>[
                'file'=>[
                    'name'=>'Изображение',
                    'type'=>'file',
                    'is_default'=>true,
                    'exts'=>['png','jpg','jpeg','svg','gif'],
                    'x_size'=>200,
                    'y_size'=>150,
                ],
                'gallery'=>[
                    'name'=>'Галерея',
                    'type'=>'gallery',
                    'is_default'=>false,
                    'exts'=>['png','jpg','jpeg','svg','gif'],
                    'x_size'=>200,
                    'y_size'=>150,
                ],
            ],
        ],
        'tab3'=>[
            'name'=>'SEO',
            'fields'=>[
                'meta_title'=>[
                    'name'=>'Title',
                    'type'=>'text',
                ],
            ],
        ],
        'tab4'=>[
            'name'=>'Характеристики',
            'function'=>true,
        ]
    ],
    'fieldsTds'=>[
        'name'=>[
            'name'=>'Наименование',
            'function'=>'editTD',
            'field_visible'=>'name',
            'order'=>true,
            'filter'=>true,
        ],
        'select1'=>[
            'name'=>'Список 1',
            'function'=>'selectTD',
            'field_visible'=>'select1',
            'order'=>false,
            'filter'=>true,
        ],
        'select4'=>[
            'name'=>'Список 4',
            'function'=>'selectTD',
            'field_visible'=>'select4',
            'order'=>false,
            'filter'=>true,
        ],
        'checkbox'=>[
            'name'=>'Галочка',
            'function'=>'checkboxTD',
            'field_visible'=>'checkbox',
            'order'=>true,
            'filter'=>false,
        ],
        'order'=>[
            'name'=>'Порядок',
            'function'=>'inputTD',
            'field_visible'=>'order',
            'order'=>true,
            'filter'=>false,
        ],
        'is_show'=>[
            'name'=>'Показать',
            'function'=>'yesnoTD',
            'field_visible'=>'is_show',
            'order'=>false,
            'filter'=>false,
        ],
    ],
    'settings'=>[
        'is_groups'=>[
            'name'=>'Создавать группы',
            'type'=>'checkbox',
            'default'=>true,
        ],
        'is_select3'=>[
            'name'=>'Показать список 3',
            'type'=>'checkbox',
            'default'=>true,
        ],
        'onPage'=>[
            'name'=>'На странице',
            'type'=>'int',
            'default'=>10,
        ],
        'showTab4'=>[
            'name'=>'Показывать хар-ки',
            'type'=>'checkbox',
            'default'=>false,
        ],
        'is_multipids'=>[
            'name'=>'Множество предков',
            'type'=>'checkbox',
            'default'=>true,
        ],
    ],
    'actions'=>[
        'edit',
        'delete'
    ],
    'fields_group'=>[
        'tab1'=>[
            'name'=>'Информация',
            'fields'=>[
                'name'=>[
                    'name'=>'Текстовое поле',
                    'type'=>'text',
                    'required'=>true,
                    'is_default'=>true,
                ],
                'url'=>[
                    'name'=>'URL',
                    'type'=>'url',
                    'is_default'=>true,
                ],
                'pid'=>[
                    'name'=>'Категории',
                    'type'=>'pid',
                    'is_one'=>1,
                ],
                'is_show'=>[
                    'name'=>'Показать',
                    'type'=>'yesno',
                ],
                'textarea'=>[
                    'name'=>'Текст (многоcтрочный)',
                    'type'=>'textarea',
                ],
                'ckeditor'=>[
                    'name'=>'Редактор',
                    'type'=>'ckeditor',
                ],
            ]
        ],
        'tab2'=>[
            'name'=>'Изображения',
            'fields'=>[
                'file'=>[
                    'name'=>'Изображение',
                    'type'=>'file',
                ],
            ],
        ],
        'tab3'=>[
            'name'=>'SEO',
            'fields'=>[
                'meta_title'=>[
                    'name'=>'Title',
                    'type'=>'text',
                ],
            ],
        ],
    ],
];
