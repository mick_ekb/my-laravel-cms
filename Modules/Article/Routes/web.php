<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/article', 'middleware' => ['auth']], function(){
    Route::get('/', 'ArticleController@index');
    Route::match(['get', 'post'], '/show/{pid?}/{page_num?}', 'ArticleController@show');
    Route::post('/openNode', 'ArticleController@openNode');
    Route::post('/showToggle/{pid?}', 'ArticleController@showToggle');
    Route::post('/searchGroup', 'ArticleController@searchGroup');
    Route::post('/searchGroupAutocomplete', 'ArticleController@searchGroupAutocomplete');

    Route::match(['get', 'post'], '/add_group/{pid?}', 'ArticleController@add_group');
    Route::match(['get', 'post'], '/add/{pid?}', 'ArticleController@add');
    Route::match(['get', 'post'], '/edit_group/{pid?}', 'ArticleController@edit_group');
    Route::match(['get', 'post'], '/edit/{pid?}', 'ArticleController@edit');
});
