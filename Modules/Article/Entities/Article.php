<?php

namespace Modules\Article\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Base;


class Article extends Base
{
    use HasFactory;

    protected $fillable = [];

    protected $controllerName = 'Article';
    protected $isGroups=true;

    protected $extraPidsTable = 'test_pids';
    protected $extraPidsField = 'tests_id';
    protected $extraPidsGroupField = 'test_groups_id';


    protected static function newFactory()
    {
        return \Modules\Article\Database\factories\ArticleFactory::new();
    }
}
