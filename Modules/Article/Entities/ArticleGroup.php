<?php

namespace Modules\Article\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Base;

class ArticleGroup extends Base
{
    use HasFactory;

    protected $fillable = [];

    protected $controllerName = 'Article';
    protected $isGroups=true;

    protected $extraPidsTable = 'articles_pids';
    protected $extraPidsField = 'articles_id';
    protected $extraPidsGroupField = 'articles_group_id';

    protected static function newFactory()
    {
        return \Modules\Article\Database\factories\ArticleGroupFactory::new();
    }
}
